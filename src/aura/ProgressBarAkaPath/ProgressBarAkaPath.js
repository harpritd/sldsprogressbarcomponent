({
    fireComponentEvent : function(cmp, event) {
        var cmpEvent = cmp.getEvent("ProgressBarClickEvent");
        console.log(" Message from fireComponentEvent");
        cmpEvent.setParams({
            "Stage" : "The user has clicked on " + cmp.get("v.stages")[event.target.id],
            "Index" : event.target.id
        });
        cmpEvent.fire();
    }
})