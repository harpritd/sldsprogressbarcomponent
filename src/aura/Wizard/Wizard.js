({
    handleComponentEvent : function(cmp, event) {
        var Index = event.getParam("Index");
        var Stage = event.getParam("Stage");
        cmp.set("v.messageFromClickEvent", Stage+' having index '+Index);
    }
})